module.exports = function(grunt) {

  grunt.initConfig({

    // Watches for changes and runs tasks
    // Livereload is setup for the 35729 port by default
    watch: {
      sass: {
        files: ['sass/**/*.sass'],
        tasks: ['sass:dev', 'autoprefixer'],
        options: {
          //livereload: 35729,
          spawn: false
        }
      },
      php: {
        files: ['**/*.php'],
        options: {
          //livereload: 35729,
          spawn: false
        }
      }
    },

    // Sass object
    sass: {
      production : {
        files : [
          {
            src: ['**/*.sass', '!**/_*.sass'],
            cwd: 'sass',
            dest: 'css',
            ext: '.css',
            expand: true
          }
        ],
        options: {
          style: 'compressed',
          compass: true
        }
      },
      dev: {
        files: [
          {
            src: ['**/*.sass', '!**/_*.sass'],
            cwd: 'sass',
            dest: 'css',
            ext: '.css',
            expand: true
          }
        ],
        options: {
          style: 'expanded',
          unixNewlines: false, // tenzij je op linux werkt
          compass: true,
          precision: 8,
          require: 'bootstrap-sass'
        }
      }
    },
    
    // Auto prefixer settings for browser prefixes
    autoprefixer: {
      options: {
        browsers: ['last 3 versions']
      },
      dist: {
        files: [{
          expand: true,
          cwd: 'css',
          src: '**/*.css',
          dest: 'css'
        }]
      }
    },

  });

  // Default task
  grunt.registerTask('default', ['watch']);

  // Build task
  grunt.registerTask('build', ['sass:production', 'autoprefixer']);

  // Load up tasks
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-autoprefixer');

};