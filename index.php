<?php
/**
 * INDEX.PHP - Blog / nieuws overzicht
 */

get_header();

if ( have_posts() ) :

	// the loop
	while ( have_posts() ) : the_post();
?>

		<article id="post-<?php the_ID();?>" class="post">

			<header>
<?php
				if ( has_post_thumbnail() ) :
?>
					<div class="featured-image">
						<a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>">
<?php
							the_post_thumbnail();
?>
						</a>
					</div>
<?php
				endif;
?>
				<div class="title">
					<a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>">
<?php
						the_title();
?>
					</a>
				</div>
				
				<span class="date">
<?php
					the_date('d-m-Y');
?>
				</span>

			</header>

			<div class="content">
<?php
				the_excerpt();
?>
			</div>

		</article>

<?php
	endwhile;
	// the loop ends

else:
?>
	<p>
		Niets gevonden.
	</p>
<?php
endif;

get_footer();