<?php
/**
 * FRONT-PAGE.PHP - De homepagina
 */

get_header();

// can loop?
if ( have_posts() ) :

	// the loop
	while ( have_posts() ) : the_post();
?>

		<article id="post-<?php the_ID();?>" class="post">

			<header>
<?php
				if ( has_post_thumbnail() ) :
?>
					<div class="featured-image">
<?php
						the_post_thumbnail();
?>
					</div>
<?php
				endif;
?>
				<div class="title">
<?php
					the_title();
?>
				</div>
				
				<span class="date">
<?php
					the_date('d-m-Y');
?>
				</span>

			</header>

			<div class="content">
<?php
				the_excerpt();
?>
			</div>

		</article>

<?php
	endwhile;
	// the loop ends

// no loop
else:
?>
	<p>
		Niets gevonden.
	</p>
<?php
endif;

get_footer();