<?php

/**
 * Proper way to enqueue scripts and styles
 */
function theme_name_scripts() {
	
	wp_enqueue_style( 'style-name', get_template_directory_uri() . '/css/style.css' );
	//wp_enqueue_script( 'script-name', get_template_directory_uri() . '/js/example.js', array(), '1.0.0', true );
	
}

add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );

/**
 * Titel fix
 */

add_filter( 'wp_title', 'baw_hack_wp_title_for_home' );

function baw_hack_wp_title_for_home( $title ) {
	
	if( empty( $title ) && ( is_home() || is_front_page() ) ) {
		
		return get_bloginfo( 'name' );
		
	} else {
	
		return $title . ' - ' . get_bloginfo( 'name' );
	}
	
	return $title;
}