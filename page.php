<?php
/**
 * PAGE.PHP - Pagina's
 */

get_header();

if ( have_posts() ) :

	// the loop
	while ( have_posts() ) : the_post();

		// contentblokken plaatsen?
		// vanaf hier...

?>

		<article id="post-<?php the_ID();?>" class="post">

			<header>
<?php
				if ( has_post_thumbnail() ) :
?>
					<div class="featured-image">
<?php
						the_post_thumbnail();
?>
					</div>
<?php
				endif;
?>
				<div class="title">
<?php
					the_title();
?>
				</div>
				
				<span class="date">
<?php
					the_date('d-m-Y');
?>
				</span>

			</header>

			<div class="content">
<?php
				the_excerpt();
?>
			</div>

		</article>

<?php

		// contentblokken plaatsen?
		// ...tot hier

	endwhile;
	// the loop ends

else:
?>
	<p>
		Niets gevonden.
	</p>
<?php
endif;

get_footer();